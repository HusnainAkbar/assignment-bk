const express = require("express");
const fs = require('fs');
const connection = require("./connection");
const storage = require("./storage");
const {
  v4: uuidv4,
} = require('uuid');
var app = express();
const bodyParser= require('body-parser');
var cors = require('cors');
const Papa = require('papaparse');
app.use(bodyParser.urlencoded({extended: true}));

app.listen(3000, () => {
 console.log("Server running on port 3000");
});

const upload = storage.upload;
const mySqlCon = connection.connectToSql();

app.get("/invoices", cors(), (req, res, next) => {
    mySqlCon.query("SELECT * FROM invoices", function (err, result, fields) {
        if (err) throw err;
        res.json([result]);
      });
});

app.post('/upload', cors(), upload.single('file'), async function (req, res) {
  const fileRows = [];
  
  let data = await readCSVFile(req.file.path);
  data.data.forEach(row => {
    const uploadDate = new Date(Date.now());
    row['Upload Date'] = `${uploadDate.getMonth() + 1}/${uploadDate.getDate()}/${uploadDate.getFullYear()}`
    const diffsByDays = getDifferenceInDays(new Date(row['Due Date']).getTime(), new Date(row['Upload Date']).getTime());
    const coefficient = diffsByDays === -1 ? 0 : diffsByDays > 30 ? 0.5 : 0.3
    row['Selling Price'] = Math.floor(row.Amount * coefficient);
    // let postQuery = 'INSERT INTO `practice`.`invoices` (`id`, `internal_invoice_id`, `due_date`, `upload_date`, `amount`, `selling_price`) VALUES ('PKO123123KODFK', '12b', '\'3/26/2022\'', '\'9/26/2021\'', '3625', '205');'
    let sql = `INSERT INTO invoices (id, internal_invoice_id, due_date, upload_date, amount, selling_price) VALUES (UUID(), '${row['Internal Invoice ID']}', '${row['Due Date']}', '${row['Upload Date']}', ${row['Amount']}, ${row['Selling Price']})`;
    mySqlCon.query(sql, function (err, result) {  
      if (err) throw err;
      console.log("1 record inserted");  
    });
  }) 
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
   
  res.send({
    resp: 'success',
    message: 'data added successfully'
  })
});

const readCSVFile = (filePath) => {
  try {
      const msgFileBuffer = fs.readFileSync(filePath, {encoding: "utf8"}); 
      
      const config = {
          header: true,
          skipEmptyLines: true,
          transformHeader: function (h) {
              return h.trim();
          },
      };

      const record = Papa.parse(msgFileBuffer.toString("utf-8"), config);
      return record;
  } catch (error) {
      console.log(error);
  }
};

const getDifferenceInDays = (date1, date2) => {
  const diffInMs = date1 - date2 > -1 ? date1 - date2 : -1;
  if (diffInMs <= -1) {
    return -1;
  } 
  const diffInDays = Math.ceil(diffInMs / (1000 * 60 * 60 * 24));
  return diffInDays;
}
