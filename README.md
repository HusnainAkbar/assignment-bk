# Assignment-BK

## Getting started

- Take clone of this repository.
- Move inside **assignment-bk**.

```
cd assignment-bk
```

- Install node modules.

```
npm install
```

## Setup MySQL

- It is recommended to install **MySQL Workbench 8.0** from [Link to Google](https://dev.mysql.com/downloads/workbench/).
- Import MySQL Dump file given in **invoices_db** folder, in your **MySQL Workbench 8.0**. This will import DB schema inside you workbench.
- Install **Xampp** for local MySQL server from [Link to Google](https://www.apachefriends.org/download.html).
- After installing **Xampp** run MySQL server.

## Run App

- Run node command.

```
node app.js
```
