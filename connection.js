var mysql = require('mysql');
const host = '127.0.0.1';
const user = 'root';
const password = '';
const database = 'practice';
var connection = mysql.createConnection({
  host,
  user,
  password,
  database
});
 
function connectToSql() {
    connection.connect();
    return connection;
}

function terminateConnection() {
    connection.end();
}
 
module.exports = {
    connectToSql,
    terminateConnection
}